
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

    public static int yesterday = 1;
    int tomorrow = 10;

    public static void main(String[] args){
        // find factorial of number.
        int a = 5;
        int fact = getFactbyLoop(a);
        //System.out.println(fact);
        int fact1 = getFactbyRec(a);
        //System.out.println(fact1);
        //String[] input = {"geeksforgeeks", "geeks", "geek", "geezer"};
       // System.out.println(longestCommonPrefix(input));
        Thread threadOne = new Thread(() -> System.out.println("Hello from thread one"));
        threadOne.start();
        int[] alphanumericArray = {'a', '1', 'b', '2', 'c', '3', 'd', '4', 'e', '5'};
        Arrays.stream(alphanumericArray).mapToObj(value -> value != -1).forEach(System.out::println);
        Arrays.stream(alphanumericArray).filter(Character :: isDigit).forEach(System.out::println);
        String[] str = {"1", "4"};
        Arrays.stream(str).mapToInt(Integer::valueOf).forEach(System.out :: println);
//        Arrays.stream(alphanumericArray)
//                .filter(Character::isDigit)
//                .forEach(System.out::println);
        List<Integer> numbers = Arrays.asList(1, 4, 8, 40, 11, 22, 33, 99);
        numbers.stream().filter(integer -> integer %2 == 0).collect(Collectors.toList());

        Test tolls = new Test();
        int today=20, tomorrow = 40;
        System.out.print(today + tolls.tomorrow + tolls.yesterday);
    }

    private static int getFactbyLoop(int a) {
        int temp = 1;
        for(int i = a; i > 1; i--){
            temp = temp * i;
        }
        return temp;
    }

    private static int getFactbyRec(int a){
        if(a < 1){
            return a;
        }
        return a * getFactbyLoop(a - 1);
        //return 0;
    }

    private static String longestCommonPrefix(String[] arr)
    {
        int n = arr.length;
        // take temp_prefix string and assign first element of arr : a.
        String result = arr[0];

        // Iterate for rest of element in string.
        for(int i = 1; i < n; i++){
            // .indexOf() return index of that substring from string.
            while(arr[i].indexOf(result) != 0){

                // update matched substring prefx
                result = result.substring(0, result.length()-1);

                // check for empty case. direct return if true..
                if(result.isEmpty()){
                    return "-1";
                }
            }
        }
        return result;
    }


}
