import java.io.*;
import java.util.*;

public class ExternalSort1 {

    public static void main(String[] args) throws IOException {
        String inputFileName = "/home/virendra/Extra/Personal/large_input.txt"; // Replace with your input file path
        String outputFileName = "/home/virendra/Extra/Personal/sorted_output.txt"; // Replace with your output file path
        int chunkSize = 1;//256 * 1024 * 1024;//15 1000000; // Adjust the chunk size as needed (number of integers to fit in memory)

        // Step 1: Divide the input file into chunks and sort each chunk
        List<String> chunkFileNames = createSortedChunks(inputFileName, chunkSize);
        chunkFileNames.stream().distinct();
        // Step 2: Merge the sorted chunks into the output file
        mergeSortedChunks(chunkFileNames, outputFileName);

        // Step 3: Clean up temporary chunk files
        for (String chunkFileName : chunkFileNames) {
            File file = new File(chunkFileName);
            file.delete();
        }

        System.out.println("External sorting complete.");
    }

    private static List<String> createSortedChunks(String inputFileName, int chunkSize) throws IOException {
        List<String> chunkFileNames = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        String line;
        int chunkNumber = 0;
        line = br.readLine();
        while (line != null) {
            List<Integer> chunk = new ArrayList<>();
             // 50 L elements in one chunk = 2 GB
            // Read and store integers until the chunk size is reached
            while (line != null && chunk.size() < chunkSize) {
                String[] tempLine = line.split(",");
                for (String s : tempLine) {
                    if(!s.isEmpty()){
                        chunk.add(Integer.parseInt(s));
                    }
                }
                line = br.readLine();
            }

            // Sort the chunk in memory
            chunk.sort(Integer::compare);
           // chunk.forEach(integer -> System.out.println(integer));
            // Write the sorted chunk to a temporary file
            String chunkFileName = "chunk" + chunkNumber + ".txt";
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(chunkFileName))) {
                for (int num : chunk) {
                    bw.write(Integer.toString(num));
                    bw.newLine();
                }
            }

            chunkFileNames.add(chunkFileName);
            chunkNumber++;
        }

        br.close();
        return chunkFileNames;
    }

    private static void mergeSortedChunks(List<String> chunkFileNames, String outputFileName) throws IOException {
        List<BufferedReader> readers = new ArrayList<>();

        // Initialize readers for each chunk
        for (String chunkFileName : chunkFileNames) {
            readers.add(new BufferedReader(new FileReader(chunkFileName)));
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName))) {
            List<Integer> currentValues = new ArrayList<>();

            // Initialize currentValues with the first integer from each reader
            for (BufferedReader reader : readers) {
                String line = reader.readLine();
                if (line != null) {
                    currentValues.add(Integer.parseInt(line));
                }
            }
            while (!currentValues.isEmpty()) {
                // Find the smallest integer among the current values
                int smallestValue = Collections.min(currentValues);

                // Write the smallest integer to the output file
                writer.write(Integer.toString(smallestValue));
                writer.write(",");
                // Find the reader associated with the smallest integer and read the next integer
                for (int i = 0; i < readers.size(); i++) {
                    BufferedReader reader = readers.get(i);
                    if (currentValues.get(i) == smallestValue) {
                        String nextLine = reader.readLine();
                        if (nextLine != null) {
                            currentValues.set(i, Integer.parseInt(nextLine));
                        } else {
                            currentValues.remove(i);
                            readers.remove(i);
                            i--; // Adjust the index after removal
                        }
                        break; // Move to the next smallest integer
                    }
                }
            }
        } finally {
            // Close all readers
            for (BufferedReader reader : readers) {
                if (reader != null) {
                    reader.close();
                }
            }
        }
    }

    private static String readLine(BufferedReader br) throws IOException {
        return br.readLine();
    }

}
